import Vue from 'vue';
import Vuetify from 'vuetify';
// import * as VueGoogleMaps from '~/node_modules/vue2-google-maps/src/main';
import VueTour from 'vue-tour';
// You could use your own fancy-schmancy custom styles.
// We'll use the defaults here because we're lazy.
import 'vue-tour/dist/vue-tour.css';
import InstantSearch from 'vue-instantsearch';
import AsyncComputed from 'vue-async-computed';
import Vuelidate from 'vuelidate';
// import firebase from 'firebase';
// import VueFire from 'vuefire';
import VueFirestore from 'vue-firestore';

Vue.use(VueFirestore);
// Vue.use(VueFire);
Vue.use(Vuelidate);
Vue.use(InstantSearch);
Vue.use(AsyncComputed);
Vue.use(VueTour);
// Vue.use(VueGoogleMaps, {
//   load: {
//     key: '',
//     libraries: 'places' // necessary for places input
//   }
// });

Vue.use(Vuetify);
