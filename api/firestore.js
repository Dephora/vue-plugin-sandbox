import { firebase } from '@firebase/app';
import '@firebase/firestore';

const firebaseApp = firebase.initializeApp({
  apiKey: 'AIzaSyBPivI6H6kPhMtL6bh_CAHkEsggKgtRbVQ',
  authDomain: 'reptiles-d91b1.firebaseapp.com',
  databaseURL: 'https://reptiles-d91b1.firebaseio.com',
  projectId: 'reptiles-d91b1',
  storageBucket: '',
  messagingSenderId: '697005125577'
});

export const db = firebaseApp.firestore();
